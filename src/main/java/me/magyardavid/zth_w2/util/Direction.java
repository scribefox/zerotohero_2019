package me.magyardavid.zth_w2.util;

import javax.annotation.Nonnull;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * All rights reserved.
 *
 * @author Magyar Dávid
 * @since 2019. 08. 25.
 */
public enum Direction {
    LEFT,
    RIGHT,
    TOP,
    BOTTOM;

    public static final @Nonnull List<Direction> FOUR_DIRECTIONS = Collections.unmodifiableList(
            Stream.of(TOP, LEFT, BOTTOM, RIGHT).collect(Collectors.toList()));

    public int getModX() {
        switch (this) {
            case LEFT:
                return -1;
            case RIGHT:
                return +1;
            default:
                return 0;
        }
    }

    public int getModY() {
        switch (this) {
            case TOP:
                return +1;
            case BOTTOM:
                return -1;
            default:
                return 0;
        }
    }
}
