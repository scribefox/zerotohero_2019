package me.magyardavid.zth_w2.util;

import javax.annotation.Nonnegative;
import javax.annotation.Nonnull;

/**
 * All rights reserved.
 *
 * @author Magyar Dávid
 * @since 2019. 08. 25.
 */
public class BitMatrix {

    private final int width;
    private final int height;

    private final @Nonnull int[] bits;

    public BitMatrix(final @Nonnegative int width, final @Nonnegative int height) {
        this.width = width;
        this.height = height;
        bits = new int[(int) Math.ceil((width * height) / 32.0)];
    }

    public @Nonnegative int getWidth() {
        return width;
    }

    public @Nonnegative int getHeight() {
        return height;
    }

    public boolean get(final @Nonnegative int index) {
        return ((bits[index / 32] >> (index % 32)) & 1) == 1;
    }

    public boolean get(final @Nonnegative int x, final @Nonnegative int y) {
        return get(y * width + x);
    }

    public boolean flip(final @Nonnegative int x, final @Nonnegative int y) {
        boolean newValue = !get(x, y);
        set(x, y, newValue);
        return newValue;
    }

    public void set(final @Nonnegative int index, final boolean value) {
        if (value)
            bits[index / 32] |= 1 << index % 32;
        else
            bits[index / 32] &= ~(1 << index % 32);
    }

    public void set(final @Nonnegative int x, final @Nonnegative int y, final boolean value) {
        set(y * width + x, value);
    }

    public boolean put(final @Nonnegative int index, final boolean value) {
        final boolean oldValue = get(index);
        if (oldValue != value)
            set(index, value);

        return oldValue;
    }

    public boolean put(final @Nonnegative int x, final @Nonnegative int y, final boolean value) {
        final boolean oldValue = get(x, y);
        if (oldValue != value)
            set(x, y, value);

        return oldValue;
    }

    public void setAll(final boolean value) {
        if (value) {
            final int ones = ~0;
            for (int i = 0; i < bits.length; ++i)
                bits[i] = ones;
        } else {
            for (int i = 0; i < bits.length; ++i)
                bits[i] = 0;
        }
    }
}
