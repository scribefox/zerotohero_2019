package me.magyardavid.zth_w2.util;

import javax.annotation.Nonnegative;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Objects;

/**
 * All rights reserved.
 *
 * @author Magyar Dávid
 * @since 2019. 08. 16.
 */
public final class CSVReader implements AutoCloseable, Iterator<String>, Iterable<String> {

    /**
     * The reader channel to the file or other kind of input stream.
     */
    private final @Nonnull InputStreamReader reader;

    /**
     * Builder for a CSV value.
     */
    private final @Nonnull StringBuilder valueBuilder = new StringBuilder();

    /**
     * Buffer of characters to be read from the input stream at once.
     */
    private final @Nonnull CharBuffer buffer = CharBuffer.allocate(128);

    /**
     * The character, which separates the values. Usually comma.
     */
    private final char separator;

    /**
     * The amount of characters buffered into the buffer currently.
     */
    private int bufferedCharacters = 0;

    /**
     * Next value of the CSV structure. It is always buffered ahead.
     */
    private @Nullable String next$;

    /**
     * New line character.
     */
    @SuppressWarnings("WeakerAccess")
    public static final char NEW_LINE = '\n';

    /**
     * Comma character.
     */
    @SuppressWarnings("WeakerAccess")
    public static final char COMMA = ',';

    private CSVReader(final @Nonnull InputStreamReader reader, final char separator) {
        this.separator = separator;
        this.reader = Objects.requireNonNull(reader);
        buffer.position(buffer.capacity());
        this.bufferNext();
    }

    /**
     * Buffers the next value from the input stream through the {@link #reader}.
     *
     * @return {@code true} if value was terminated by a {@link #separator} character.
     */
    private boolean bufferNext() {
        if (bufferedCharacters == -1) {
            next$ = null;
            return false;
        }

        try {
            while (true) {
                if (!buffer.hasRemaining()) {
                    buffer.position(0);
                    buffer.clear();
                    bufferedCharacters = reader.read(buffer);
                    buffer.rewind(); //flip read/write mode
                }

                if (bufferedCharacters <= 0) {
                    bufferedCharacters = -1;
                    return false;
                }

                char character$;
                for (@Nonnegative int bufferedCharIdx$ = buffer.position(); bufferedCharIdx$ < bufferedCharacters; ++bufferedCharIdx$) {
                    character$ = buffer.get(bufferedCharIdx$); //.charAt(idx) would start from .position!
                    if (character$ != separator)
                        valueBuilder.append(character$);
                    else {
                        next$ = valueBuilder.toString();
                        valueBuilder.setLength(0); //clear
                        buffer.position(bufferedCharIdx$ + 1);
                        return true;
                    }
                }

                buffer.position(buffer.capacity());
            }
        } catch (final @Nonnull IOException ioe) {
            ioe.printStackTrace();
            try {
                this.close();
            } catch (final @Nonnull Exception e) {
                e.printStackTrace();
            }

            return false;
        }
    }

    public static CSVReader open(final @Nonnull File file, final char entrySeparator) throws IOException {
        final @Nonnull InputStreamReader reader = new InputStreamReader(new FileInputStream(file), Charset.forName("utf8"));
        return new CSVReader(reader, entrySeparator);
    }

    public static CSVReader open(final @Nonnull File file) throws IOException {
        return open(file, COMMA);
    }

    public static CSVReader openLines(final @Nonnull File file) throws IOException {
        return open(file, NEW_LINE);
    }

    @Override
    public boolean hasNext() {
        return next$ != null;
    }

    @Override
    public @Nonnull String next() {
        if (next$ == null)
            throw new NoSuchElementException("No more elements present.");

        final @Nonnull String current = next$;

        if (bufferedCharacters == -1) {
            next$ = null;
            return current;
        }

        if (!bufferNext()) {
            try {
                this.close();
            } catch (final @Nonnull Exception e) {
                e.printStackTrace();
            }
        }

        return current;
    }

    @Override
    public void close() throws Exception {
        reader.close();
    }

    @Override
    public @Nonnull Iterator<String> iterator() {
        return this;
    }
}
