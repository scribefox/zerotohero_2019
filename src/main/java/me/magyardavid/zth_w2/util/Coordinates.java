package me.magyardavid.zth_w2.util;

import jdk.nashorn.internal.ir.annotations.Immutable;

import javax.annotation.Nonnull;

/**
 * All rights reserved.
 *
 * @author Magyar Dávid
 * @since 2019. 08. 25.
 */
@Immutable
public class Coordinates {

    private final int x, y;

    public Coordinates(final int x, final int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    @Override
    public int hashCode() {
        return x ^ (y << 16) ^ (y >> 8);
    }

    @Override
    public boolean equals(final @Nonnull Object object) {
        return object instanceof Coordinates && equals((Coordinates) object);
    }

    public boolean equals(final @Nonnull Coordinates another) {
        return another.x == this.x && another.y == this.y;
    }

}
