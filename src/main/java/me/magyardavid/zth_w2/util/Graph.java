package me.magyardavid.zth_w2.util;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.NotThreadSafe;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Optional;

/**Not directional.
 * @param <T>
 */
@NotThreadSafe
public class Graph<T> {

    private final HashMap<T, HashSet<T>> ways = new HashMap<>();

    private @Nonnull HashSet<T> getOrCreateConnections(final@Nonnull T object) {
        return ways.computeIfAbsent(object, key -> new HashSet<T>());
    }

    private @Nonnull Optional<HashSet<T>> getConnections(final@Nonnull T object) {
        return Optional.ofNullable(ways.get(object));
    }

    public void register(final T object) {
        getOrCreateConnections(object);
    }

    public void register(final T a, final T b) {
        getOrCreateConnections(a).add(b);
        getOrCreateConnections(b).add(a);
    }

    public void unregister(final T object) {
        if (ways.remove(object) != null) {
            ways.values().forEach(it -> it.remove(object));
        }
    }

    public void unregister(final T a, final T b) {
        getConnections(a).ifPresent(it -> it.remove(b));
        getConnections(b).ifPresent(it -> it.remove(a));
    }

    public boolean isRegistered(final T object) {
        return ways.containsKey(object);
    }

    public boolean isRegistered(final T a, final T b) {
        return getConnections(a)
                .map(it -> it.contains(b))
                .orElse(false);
    }

}
