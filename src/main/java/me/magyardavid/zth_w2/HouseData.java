package me.magyardavid.zth_w2;

import jdk.nashorn.internal.ir.annotations.Immutable;

import javax.annotation.Nonnegative;
import javax.annotation.Nonnull;

/**
 * All rights reserved.
 *
 * @author Magyar Dávid
 * @since 2019. 08. 25.
 */
@SuppressWarnings("WeakerAccess")
@Immutable
public final class HouseData {

    /**
     * How costly it is to walk to the house.
     */
    private final double weight;

    /**
     * Levels of the house.
     */
    private final @Nonnegative byte levels;

    private final @Nonnull HouseType type;

    private final @Nonnull HouseColor color;

    private final @Nonnull FenceType fence;

    public HouseData(
            final @Nonnull HouseType type,
            final @Nonnull HouseColor color,
            final @Nonnull FenceType fenceType,
            final @Nonnegative byte levels,
            final double weight) {

        this.type = type;
        this.color = color;
        this.fence = fenceType;
        this.levels = levels;
        this.weight = weight;
    }

    /**
     * Returns how difficult it is to get to the house from a neightbouring block (sharing sides).
     */
    public double getWeight() {
        return this.weight;
    }

    public static @Nonnull HouseData parse(final @Nonnull String string) throws NumberFormatException {
        final @Nonnull String[] parts = string.split(",");
        return new HouseData(
                HouseType.parse(parts[0].trim()),
                HouseColor.parse(parts[1].trim()),
                FenceType.parse(parts[2].trim()),
                HouseLevel.parse(parts[3].trim()),
                Integer.parseInt(parts[4].trim()));
    }

    public byte getLevels() {
        return levels;
    }

    @Nonnull public HouseType getType() {
        return type;
    }

    @Nonnull public HouseColor getColor() {
        return color;
    }

    @Nonnull public FenceType getFence() {
        return fence;
    }
}
